#[cfg(target_feature="aes")]
use core::arch::x86_64::*;

pub unsafe trait Lane: Copy + AsRef<[u8]> {
    fn from_bytes(buf: &[u8]) -> Self;
    fn copy(&mut self, buf: &[u8]);

    fn merge(&mut self, other: Self);
    fn rotate(&mut self, other: &mut Self);

    fn load(&mut self, buf: &[u8]) {
        let other = Self::from_bytes(buf);
        self.merge(other);
    }
}

#[cfg(target_feature="aes")]
unsafe fn load16<T>(ptr: &T) -> __m128i {
    _mm_loadu_si128(ptr as *const _ as *const _)
}

#[repr(C)] #[derive(Clone, Copy)]
#[cfg(target_feature="aes")]
struct AESNIx1 { l0: __m128i, l1: __m128i, l2: __m128i, l3: __m128i }

#[cfg(target_feature="aes")]
impl AsRef<[u8]> for AESNIx1 {
    fn as_ref(&self) -> &[u8] {
        unsafe {
            core::slice::from_raw_parts(&self.l0 as *const _ as *const _, 256)
        }
    }
}

#[cfg(target_feature="aes")]
unsafe impl Lane for AESNIx1 {
    fn from_bytes(buf: &[u8]) -> Self {
        assert!(buf.len() == 64);
        unsafe {
            AESNIx1 {
                l0: load16(&buf[0]),
                l1: load16(&buf[16]),
                l2: load16(&buf[32]),
                l3: load16(&buf[48]),
            }
        }
    }

    fn copy(&mut self, buf: &[u8]) {
        let start = &mut self.l0 as *mut _ as *mut u8;
        let len = core::cmp::min(buf.len(), 256);
        unsafe {
            buf.as_ptr().copy_to_nonoverlapping(start, len)
        }
    }

    fn merge(&mut self, other: Self) {
        unsafe {
            self.l0 = _mm_aesdec_si128(self.l0, other.l0);
            self.l1 = _mm_aesdec_si128(self.l1, other.l1);
            self.l2 = _mm_aesdec_si128(self.l2, other.l2);
            self.l3 = _mm_aesdec_si128(self.l3, other.l3);
        }
    }

    fn rotate(&mut self, other: &mut Self) {
        self.merge(*other);
        *other = AESNIx1 {
            l0: other.l1,
            l1: other.l2,
            l2: other.l3,
            l3: other.l0
        };
    }
}

fn u8s(buf: &[u64]) -> &[u8] {
    unsafe {
        core::slice::from_raw_parts(buf.as_ptr() as *mut u8, buf.len() * 8)
    }
}

fn u8sm(buf: &mut [u64]) -> &mut [u8] {
    unsafe {
        core::slice::from_raw_parts_mut(buf.as_mut_ptr() as *mut u8, buf.len() * 8)
    }
}

pub fn generic_hash<L: Lane>(seed: u64, data: &[u8]) -> L {
    let seed2 = seed + data.len() as u64 + 1;
    let iv = [
        seed, seed2, seed, seed2, seed, seed2, seed, seed
    ];

    let mut s0123 = L::from_bytes(u8s(&iv));
    let mut s4567 = s0123;
    let mut s89ab = s0123;
    let mut scdef = s0123;

    for mut chunk in data.chunks(256) {
        let mut full_buf = [0u64; 32];
        if chunk.len() != 256 {
            for i in 0..4 { (&mut full_buf[i*8..(i+1)*8]).copy_from_slice(&iv[..]) }
            u8sm(&mut full_buf)[0..chunk.len()].copy_from_slice(chunk);
            chunk = u8s(&full_buf);
        }
        s0123.load(&chunk[0..64]);
        s4567.load(&chunk[64..128]);
        s89ab.load(&chunk[128..192]);
        scdef.load(&chunk[192..]);
    }

    let mut r0 = L::from_bytes(u8s(&iv));
    
    for _ in 0..4 {
        r0.rotate(&mut s0123);
        r0.rotate(&mut s4567);
        r0.rotate(&mut s89ab);
        r0.rotate(&mut scdef);
    }

    for _ in 0..5 {
        r0.load(u8s(&iv));
    }

    r0
}

#[cfg(target_feature="aes")]
pub fn hash(seed: u64, data: &[u8]) -> impl AsRef<[u8]> {
    generic_hash::<AESNIx1>(seed, data)
}

#[cfg(not(target_feature="aes"))]
#[allow(unreachable_code)]
pub fn hash(_seed: u64, _data: &[u8]) -> impl AsRef<[u8]> {
    // appease type inference, it gets defaulted to ()?
    let _a: [u8;0] = unimplemented!();
    _a
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        let data = (0..16000).map(|x| x as u8).collect::<Vec<u8>>();
        let hash_bytes = super::hash(0, &data);
        let expected = [174, 66, 151, 129, 84, 109, 180, 119, 122, 23, 40, 127, 162, 128, 125, 138, 151, 98, 147, 163, 248, 31, 229, 100, 131, 231, 26, 23, 79, 104, 62, 10, 205, 17, 227, 73, 123, 95, 188, 235, 143, 161, 158, 104, 79, 233, 142, 97, 26, 15, 245, 163, 92, 195, 111, 183, 96, 180, 188, 204, 93, 236, 68, 227, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 0, 0, 0, 0, 0, 0, 0, 0, 129, 62, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 129, 62, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 129, 62, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 129, 62, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 129, 62, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 129, 62, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 129, 62, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 129, 62, 0, 0, 0, 0, 0, 0] ;
        assert_eq!(hash_bytes.as_ref(), &expected[..]);
    }
}
